import { useState } from "react";
import "./App.css";
import ListTodo from "./components/ListTodo";

function App() {
  const [count, setCount] = useState(0);

  return (
    <div className="App">
      <ListTodo />
    </div>
  );
}

export default App;
