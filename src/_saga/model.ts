export interface Todo {
  body: String;
  id: Number;
  title: String;
  userId: Number;
}
