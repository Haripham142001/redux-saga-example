import { call, takeEvery, put } from "redux-saga/effects";
import Axios from "axios";
import { fetchData, addDataTodo } from "./store";
import { sagaActions } from "./sagaActions";

interface paramCallApi {
  url: String | any;
  method?: String | any;
  data?: any;
}

interface Result {
  config: any;
  data: any;
  headers: any;
  request: any;
  status: Number;
  statusText: String;
}

let callAPI: any = async ({ url, method, data }: paramCallApi) => {
  return await Axios({
    url,
    method,
    data,
  });
};

export function* fetchDataSaga(): any {
  try {
    let result = yield call(() =>
      callAPI({
        url: "https://jsonplaceholder.typicode.com/todos",
        method: "GET",
        data: "",
      })
    );
    yield put(fetchData(result.data));
  } catch (e) {
    yield put({ type: "TODO_FETCH_FAILED" });
  }
}

export function* functionAddTodo(param: any): any {
  try {
    // debugger;
    let result: Result = yield call(() =>
      callAPI({
        url: "https://jsonplaceholder.typicode.com/todos",
        method: "POST",
        data: {
          title: param.payload,
          body: param.payload,
          userId: 1,
        },
      })
    );
    yield put(addDataTodo(result.data));
  } catch (error) {
    console.log(error);
    yield put({ type: "ADD_TODO_FAILED" });
  }
}

export default function* rootSaga() {
  yield takeEvery(sagaActions.FETCH_DATA_SAGA, fetchDataSaga);
  yield takeEvery(sagaActions.ADD_DATA_TODO, functionAddTodo);
}
