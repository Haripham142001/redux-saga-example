import {
  createSlice,
  configureStore,
  getDefaultMiddleware,
} from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import { Todo } from "./model";
import saga from "./saga";

interface InitialState {
  todos: [Todo] | [];
}

const initialState: InitialState = {
  todos: [],
};

const todoSlice = createSlice({
  name: "todo",
  initialState,
  reducers: {
    fetchData: (state, action) => {
      return {
        todos: action.payload,
      };
    },
    addDataTodo: (
      state,
      action: {
        payload?: Todo | undefined;
        type?: string;
      }
    ) => {
      state.todos.unshift(action.payload);
    },
  },
});

export const { fetchData, addDataTodo } = todoSlice.actions;

let sagaMiddleware = createSagaMiddleware();
const middleware = [...getDefaultMiddleware({ thunk: false }), sagaMiddleware];

const store = configureStore({
  reducer: {
    todo: todoSlice.reducer,
  },
  middleware,
});

sagaMiddleware.run(saga);

export default store;
