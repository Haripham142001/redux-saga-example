import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { sagaActions } from "../_saga/sagaActions";

export default function () {
  const dispatch = useDispatch();
  const todos = useSelector((state: any) => state.todo.todos);
  const [input, setInput] = React.useState<String>("");

  const renderList = () => {
    return todos.map((todo: any) => {
      return <p key={todo.id}>{todo.title}</p>;
    });
  };

  const handleAdd = async () => {
    dispatch({
      type: sagaActions.ADD_DATA_TODO,
      payload: input,
    });
    setInput("");
  };

  return (
    <div>
      <button onClick={() => dispatch({ type: sagaActions.FETCH_DATA_SAGA })}>
        Getdata
      </button>
      <input
        type="text"
        placeholder="Content"
        value={input}
        onChange={(e) => setInput(e.target.value)}
      />
      <button onClick={handleAdd}>Submit</button>
      {renderList()}
    </div>
  );
}
